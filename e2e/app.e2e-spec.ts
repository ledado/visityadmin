import { VisityAdminPage } from './app.po';

describe('visity-admin App', () => {
  let page: VisityAdminPage;

  beforeEach(() => {
    page = new VisityAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
