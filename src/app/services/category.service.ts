import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ServiceCategory } from '../classes/service-category';
@Injectable()
export class CategoryService {

  constructor(private http: Http) { }

  getServiceCategories(): Observable<ServiceCategory[]>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let apiUrl = "http://neposapp.com/api/";

    let params = {"jsonrpc":"2.0","method":"getServices",
      "params":{
        "apiKey":"14614261541265278",
        "params":{
          "userId":"1",
          "hotelId":"1",
          "token":"sQWYT9IPQ71472480755"
        }
      }
    }

    return this.http.post(apiUrl, params, options)
        .map(this.extractServiceCategoriesData)
        .catch(this.handleError);
  }
  private extractServiceCategoriesData(res: Response) {
    let body = res.json();
    console.log(body)
    let categories = []
    console.log(categories)
    if(body.result.result == 1){
      for(let i = 0; i < body.result.categories.length; i++){
        // console.log(body.result.services[i])
        categories.push({
          id: body.result.categories[i].id,
          title: body.result.services[i].title,
          content: body.result.services[i].content,
          serviceCategory: {
            id: body.result.services[i].serviceCategory.id,
            title: body.result.services[i].serviceCategory.title
          }
        })
      }
    }
    // console.log(services)
    return categories;
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
