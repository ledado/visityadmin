import { Component, OnInit } from '@angular/core';

import { Observable }       from 'rxjs/Observable';
import { ServiceService } from '../../services/service.service';
import { Service } from '../../classes/service';
import {ServiceCategory} from "../../classes/service-category";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  providers: [ServiceService]
})
export class ServicesComponent implements OnInit {

  title = "Services"
  services: Service[];
  editService:Service = <Service>{ //default values
    title: "title",
    id: 0,
    content: "content"

  };

  constructor(private serviceService: ServiceService) { }

  ngOnInit() {
    this.getServices();
    $('.editServiceImage').dimmer({
      on: 'hover'
    });
  }

  getServices () {
    this.serviceService.getServices().subscribe(services => this.services = services)
    console.log(this.services)
    // this.services = this.serviceService.search(term);
    // console.log(this.services)
  }

  edit(index){
    console.log(index)
    this.editService = this.services[index]
    $('.ui.modal.editServiceModal').modal('show');
  }
  save(){
    console.log(this.editService.title)
  }
}
