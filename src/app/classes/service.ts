import { ServiceCategory } from './service-category'
export class Service {

    id: number;
    title: string;
    content: string;
    serviceCategory: ServiceCategory
}
